--Create Table User
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT ,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(25) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY(id)
);

--Inserting records to the users table 
INSERT INTO users(email,password,datetime_created) VALUES("johnsmith@gmail.com","passwordA","2021-01-01 01:00:00");
INSERT INTO users(email,password,datetime_created) VALUES("juandelecruz@gmail.com","passwordB","2021-01-01 02:00:00");
INSERT INTO users(email,password,datetime_created) VALUES("janesmith@gmail.com","passwordC","2021-01-01 03:00:00");
INSERT INTO users(email,password,datetime_created) VALUES("mariadelacruz@gmail.com","passwordD","2021-01-01 04:00:00");
INSERT INTO users(email,password,datetime_created) VALUES("johndoe@gmail.com","passwordE","2021-01-01 05:00:00");

--Create post table
CREATE TABLE posts(
   id INT NOT NULL AUTO_INCREMENT,
   user_id INT NOT NULL,
   title VARCHAR(100) NOT NULL,
   content VARCHAR(100) NOT NULL,
   datetime_posted DATETIME NOT NULL,
   PRIMARY KEY(id),
   CONSTRAINT fk_users_id
   FOREIGN KEY(user_id) REFERENCES users(id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT 

);

--Inserting records in posts
INSERT INTO posts(user_id,title,content,datetime_posted)VALUES (1,"First Code","Hello World!","2021-01-02 01:00:00");
INSERT INTO posts(user_id,title,content,datetime_posted)VALUES (1,"Second Code","Hello Earth!","2021-01-02 02:00:00");
INSERT INTO posts(user_id,title,content,datetime_posted)VALUES (2,"Third Code","Welcome to Mars!","2021-01-02 03:00:00");
INSERT INTO posts(user_id,title,content,datetime_posted)VALUES (4,"Fourth Code","Bye bye solar system!","2021-01-02 04:00:00");

--Get all post with author id of 1
SELECT * FROM posts WHERE user_id = 1;

--Get all user's email and datetime of creation ;
SELECT email,datetime_created FROM users ;

--Updating a post content to Hello to the people on Earth!
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

--Delete the user with the email of johndoe@gmail.com
DELETE FROM users WHERE email = "johndoe@gmail.com";
